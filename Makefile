CC=gcc
CFLAGS=



framesize=18
varmemsize=10

mysh: shell.c interpreter.c shellmemory.c
	$(CC) $(CFLAGS) -D framesize=$(framesize) -D varmemsize=$(varmemsize)  -c shell.c interpreter.c shellmemory.c kernel.c pcb.c ready_queue.c -lpthread
	$(CC) $(CFLAGS) -o mysh shell.o interpreter.o shellmemory.o kernel.o pcb.o ready_queue.o -lpthread

clean: 
	rm mysh; rm *.o
