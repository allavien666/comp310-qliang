#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<stdbool.h>

#define SHELL_MEM_LENGTH 1000


struct memory_struct{
	char *var;
	char *value;
};

struct backing_memory{
		char *var;
		char *value;
};

// ?????????????i??????
// -1??????????????????
// ????i???????????i*3 i*3+1 i*3+2
// ??page_mapping_table[i] = j , ??i*3 -> j*3 + 100 i*3+1 -> j*3+1 + 100 i*3+2 -> j*3+2
int page_mapping_table[1000];
// ??make?????????
int page_num;

// ????????????????????0
int needSwap;



struct memory_struct shellmemory[SHELL_MEM_LENGTH];
struct backing_memory backingmemory[SHELL_MEM_LENGTH];

int* get_page_mapping_table() {
	return page_mapping_table;
}

int* get_needSwap() {
	return needSwap;
}

int get_page_num() {
	return page_num;
}
// Helper functions
int match(char *model, char *var) {
	int i, len=strlen(var), matchCount=0;
	for(i=0;i<len;i++)
		if (*(model+i) == *(var+i)) matchCount++;
	if (matchCount == len)
		return 1;
	else
		return 0;
}

char *extract(char *model) {
	char token='=';    // look for this to find value
	char value[1000];  // stores the extract value
	int i,j, len=strlen(model);
	for(i=0;i<len && *(model+i)!=token;i++); // loop till we get there
	// extract the value
	for(i=i+1,j=0;i<len;i++,j++) value[j]=*(model+i);
	value[j]='\0';
	return strdup(value);
}

// ???????????????????????
// ????????????-1
int isFull() {

	for(int i = 0;i < page_num;i ++) {
		// ???????????false
		if(strcmp(shellmemory[3*i +  100].var,"none") == 0)
			return i;
	}

	return -1;
}

// ?i?????????j???
void swap(int i , int j) {

    if(strcmp(shellmemory[j*3 + 100].var, "none") != 0) {
        printf("Page fault! Victim page contents:\n");
        printf(("\n"));
        printf("%s\n" , shellmemory[j*3 + 100].value);

    }
    if(strcmp(shellmemory[j*3 + 100 + 1].var, "none") != 0)
        printf("%s\n" , shellmemory[j*3 + 101].value);
    if(strcmp(shellmemory[j*3 + 100 + 2].var, "none") != 0)
        printf("%s\n" , shellmemory[j*3 + 102].value);
    if(strcmp(shellmemory[j*3 + 100].var, "none") != 0)
        printf("End of victim page contents.\n");






	shellmemory[3*j + 100].var = backingmemory[3*i].var;
	shellmemory[3*j + 100].value= backingmemory[3*i].value;
	shellmemory[3*j + 100 + 1].var = backingmemory[3*i + 1].var;
	shellmemory[3*j + 100 + 1].value= backingmemory[3*i + 1].value;
	shellmemory[3*j + 100 + 2].var = backingmemory[3*i + 2].var;
	shellmemory[3*j + 100 + 2].value= backingmemory[3*i + 2].value;


	page_mapping_table[i] = j;
	
}


// Shell memory functions

void mem_init(int x , int y){
	int i;
	for (i=0; i<1000; i++){		
		shellmemory[i].var = "none";
		shellmemory[i].value = "none";
        backingmemory[i].var = "none";
        backingmemory[i].value = "none";
		page_mapping_table[i] = -1;
	}
	page_num = x / 3;
	// 
	needSwap = 0;
}

// Set key value pair
void mem_set_value(char *var_in, char *value_in) {
	int i;
	for (i=0; i<1000; i++){
		if (strcmp(shellmemory[i].var, var_in) == 0){
			shellmemory[i].value = strdup(value_in);
			return;
		} 
	}

	//Value does not exist, need to find a free spot.
	for (i=0; i<1000; i++){
		if (strcmp(shellmemory[i].var, "none") == 0){
			shellmemory[i].var = strdup(var_in);
			shellmemory[i].value = strdup(value_in);
			return;
		} 
	}

	return;

}

//get value based on input key
char *mem_get_value(char *var_in) {
	int i;
	for (i=0; i<1000; i++){
		if (strcmp(shellmemory[i].var, var_in) == 0){
			return strdup(shellmemory[i].value);
		} 
	}
	return NULL;

}


void printShellMemory(){
	int count_empty = 0;
	for (int i = 0; i < SHELL_MEM_LENGTH; i++){
		if(strcmp(shellmemory[i].var,"none") == 0){
			count_empty++;
		}
		else{
			printf("\nline %d: key: %s\t\tvalue: %s\n", i, shellmemory[i].var, shellmemory[i].value);
		}
    }
	printf("\n\t%d lines in total, %d lines in use, %d lines free\n\n", SHELL_MEM_LENGTH, SHELL_MEM_LENGTH-count_empty, count_empty);
}


/*
 * Function:  addFileToMem 
 * 	Added in A2
 * --------------------
 * Load the source code of the file fp into the shell memory:
 * 		Loading format - var stores fileID, value stores a line
 *		Note that the first 100 lines are for set command, the rests are for run and exec command
 *
 *  pStart: This function will store the first line of the loaded file 
 * 			in shell memory in here
 *	pEnd: This function will store the last line of the loaded file 
 			in shell memory in here
 *  fileID: Input that need to provide when calling the function, 
 			stores the ID of the file
 * 
 * returns: error code, 21: no space left
 */
int load_file(FILE* fp, int* pStart, int* pEnd, char* filename){
	char *line;
    size_t i;
    int error_code = 0;
	bool hasSpaceLeft = false;
	bool flag = true;
	i=0;
	size_t candidate;
	
	while(flag){
		flag = false;
		for (i; i < SHELL_MEM_LENGTH; i++){
			if(strcmp(backingmemory[i].var,"none") == 0){
				*pStart = (int)i;
				hasSpaceLeft = true;
				break;
			}
		}
		candidate = i;
		for(i; i < SHELL_MEM_LENGTH; i++){
			if(strcmp(backingmemory[i].var,"none") != 0){
				flag = true;
				break;
			}
		}
	}

	i = candidate;


	// ??i?3????
	if(i % 3 == 1)i += 2;
	else if(i % 3 == 2)i += 1;

    *pStart = (int)i;


	//shell memory is full
	if(hasSpaceLeft == 0){
		error_code = 21;
		return error_code;
	}
    
	// ???????????????????
	// 0  1 2??0????
	int frameId = 0;
    for (size_t j = i; j < SHELL_MEM_LENGTH; j++){
        if(feof(fp))
        {
            *pEnd = (int)j-1;
            break;
        }else {
			line = calloc(1, SHELL_MEM_LENGTH);
			fgets(line, 999, fp);

			
			

			char* semicolon = strchr(line, ';');
			if (semicolon != NULL) {
				// Split the line into two separate strings
				*semicolon = '\0';
				semicolon++;

				// Remove whitespace from the beginning of the second string
				while (*semicolon == ' ' || *semicolon == '\t') {
					semicolon++;
				}

				backingmemory[j].var = strdup(filename);
				backingmemory[j].value = strndup(line, strlen(line));
				j++;

				backingmemory[j].var = strdup(filename);
				backingmemory[j].value = strdup(semicolon);
			} else {
				// Load the line into a single location in the shell memory
				backingmemory[j].var = strdup(filename);
				backingmemory[j].value = strdup(line);
			}

			free(line);
		}

    }

	//no space left to load the entire file into shell memory
	if(!feof(fp)){
		error_code = 21;
		//clean up the file in memory
		for(int j = 1; i <= SHELL_MEM_LENGTH; i ++){
			shellmemory[j].var = "none";
			shellmemory[j].value = "none";
    	}
		return error_code;
	}
	//printShellMemory();
    return error_code;
}



char * mem_get_value_at_line(int index){
	if(index<0 || index > SHELL_MEM_LENGTH) return NULL; 
	return shellmemory[index].value;
}

void mem_free_lines_between(int start, int end){
	for (int i=start; i<=end && i<SHELL_MEM_LENGTH; i++){
		if(shellmemory[i].var != NULL){
			free(shellmemory[i].var);
		}	
		if(shellmemory[i].value != NULL){
			free(shellmemory[i].value);
		}	
		shellmemory[i].var = "none";
		shellmemory[i].value = "none";
	}
}